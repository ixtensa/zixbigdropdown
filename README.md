
# README #

!!! IMPORTANT NOTICE !!!

* Version: Extension include scripts only for Contao >3.2, <=3.5

### What is this repository for? ###

* Big Drop-Down navigation for Contao
* Version: 1.0.0

### How do I get set up? ###

1. Copy folder to */system/modules/*.
2. Refresh Database.
3. Choose the navigation template *nav_default_zixBigDropDown*.
4. Go to page structure and choose the page you whant add big drop-down and activate the checkbox
5. Go to subpages and add your image and text.
6. You don't need to activate the checkbox by subpages.


### How it's work? ###

The extension check, is on a page the checkbox for big drop-down activated.

If it's activated will change the normal navigation to big drop-down (happen autoticaly) and add image and text to the subpages.

You can add on all subpages a image and text if the checkbox by parentpage activated.

There is a css included for the standart representation (without any font-size and font-color, just for the layout). You find it under the *system/modules/zixBigDropDown/assets/css/bigdropdown.scss* and you can custumize or overwrite it.


### Who do I talk to? ###

* Hakan Havutcuoglu
* info@havutcuoglu.com