<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   zixBigDropDown
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright Ixtensa 2015
 */


/*
 * Hooks
 */

// generatePage addCanonical
//$GLOBALS['TL_HOOKS']['generatePage'][] = array('zixBigDropDown', 'addBigDropDown' );

/**
 * Frontend scripts
 */
if (TL_MODE == 'FE') {
	// JS

	// CSS
	$GLOBALS['TL_CSS'][] = 'system/modules/zixBigDropDown/assets/css/bigdropdown.scss|static';
}

?>
