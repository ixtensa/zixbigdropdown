<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package   zixBigDropDown
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright Ixtensa 2015
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'IXTENSA',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'IXTENSA\zixBigDropDown' => 'system/modules/zixBigDropDown/classes/zixBigDropDown.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'nav_default_zixBigDropDown'	=> 'system/modules/zixBigDropDown/templates',
));