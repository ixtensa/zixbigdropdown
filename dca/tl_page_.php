<?php
// Hintergrundsbild ++++++++++++++++++++++++++++++++++++++++++++++
$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["regular"]=str_replace(
	'{expert_legend:hide}', '{extra_legend},ixPageImage;{expert_legend}', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["regular"]
);
// Interne Weiterleitung
$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["forward"]=str_replace(
	'{expert_legend:hide}', '{extra_legend},ixPageImage;{expert_legend}', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["forward"]
);
// Externe Weiterleitung (eigentlich nicht benötigt)
$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["redirect"]=str_replace(
	'{expert_legend:hide}', '{extra_legend},ixPageImage;{expert_legend}', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["redirect"]
);
// Startpunkt einer Webseite
$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["root"]=str_replace(
	'{expert_legend:hide}', '{extra_legend},ixPageImage;{expert_legend}', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["root"]
);


$GLOBALS["TL_DCA"]["tl_page"]["fields"]["ixPageImage"] = array(
	'label' => &$GLOBALS["TL_LANG"]["tl_page"]["ixPageImage"],
	'exclude' => true,
	'search' => true,
	'inputType' => 'fileTree',
	'eval' => array('filesOnly'=>true, 'extensions'=>$GLOBALS['TL_CONFIG']['validImageTypes'], 'fieldType'=>'radio', 'mandatory'=>false),
	'sql' => "binary(16) NULL"
);




// Navigationsbild ++++++++++++++++++++++++++++++++++++++++++++++
$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["regular"]=str_replace(
	'{extra_legend}', '{extra_legend},ixPageNaviImage', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["regular"]
);

$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["forward"]=str_replace(
	'{extra_legend}', '{extra_legend},ixPageNaviImage', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["forward"]
);

$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["redirect"]=str_replace(
	'{extra_legend}', '{extra_legend},ixPageNaviImage', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["redirect"]
);

$GLOBALS["TL_DCA"]["tl_page"]["fields"]["ixPageNaviImage"] = array(
	'label' => &$GLOBALS["TL_LANG"]["tl_page"]["ixPageNaviImage"],
	'exclude' => true,
	'search' => true,
	'inputType' => 'fileTree',
	'eval' => array('filesOnly'=>true, 'extensions'=>$GLOBALS['TL_CONFIG']['validImageTypes'], 'fieldType'=>'radio', 'mandatory'=>false),
	'sql' => "binary(16) NULL"
);


// Navigationstext ++++++++++++++++++++++++++++++++++++++++++++++
$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["forward"]=str_replace(
	'{extra_legend}', '{extra_legend},ixPageNaviText', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["forward"]
);
$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["redirect"]=str_replace(
	'{extra_legend}', '{extra_legend},ixPageNaviText', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["redirect"]
);
$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["regular"]=str_replace(
	'{extra_legend}', '{extra_legend},ixPageNaviText', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["regular"]
);

$GLOBALS["TL_DCA"]["tl_page"]["fields"]["ixPageNaviText"] = array(
	'label' => &$GLOBALS["TL_LANG"]["tl_page"]["ixPageNaviText"],
	'inputType' => 'textarea',
	'eval' => array('mandatory'=>false, 'rte'=>'tinyMCE', 'helpwizard'=>true),
	'explanation' => 'insertTags'
);

?>