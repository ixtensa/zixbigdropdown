<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   zixBigDropDown
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright Ixtensa 2015
 */

// Palletes
//$GLOBALS['TL_DCA']['tl_page']['palettes']['__selector__'][] = 'addBigDropDown';

// Regular pages
$GLOBALS['TL_DCA']['tl_page']['palettes']['regular'] =  str_replace('{expert_legend:hide}', '{extra_legend:hide},addBigDropDown,ixPageNaviImage,ixAlternate,ixFloating,ixPageNaviText;{expert_legend}', $GLOBALS['TL_DCA']['tl_page']['palettes']['regular']);

// Internal forward pages
$GLOBALS["TL_DCA"]["tl_page"]["palettes"]["forward"]=str_replace(
	'{expert_legend:hide}', '{extra_legend},addBigDropDown,ixPageNaviImage,ixAlternate,ixFloating,ixPageNaviText;{expert_legend}', $GLOBALS["TL_DCA"]["tl_page"]["palettes"]["forward"]
);

// Subpalettes
//$GLOBALS['TL_DCA']['tl_page']['subpalettes']['addBigDropDown'] = 'ixPageNaviImage, ixPageNaviText';

// Fields
$GLOBALS['TL_DCA']['tl_page']['fields']['addBigDropDown'] = array(
	'label'				=> &$GLOBALS['TL_LANG']['tl_page']['addBigDropDown'],
	'exclude'			=> true,
	'inputType'			=> 'checkbox',
	'eval'				=> array('mandatory'=>false, 'tl_class' => 'clr w100', 'helpwizard'=>true),
	'explanation'		=> 'addDropDownexpl',
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS["TL_DCA"]["tl_page"]["fields"]["ixPageNaviImage"] = array(
	'label'				=> &$GLOBALS['TL_LANG']['tl_page']['ixPageNaviImage'],
	'exclude'			=> true,
	'search'			=> true,
	'inputType'			=> 'fileTree',
	'eval'				=> array('filesOnly'=>true, 'extensions'=>$GLOBALS['TL_CONFIG']['validImageTypes'], 'fieldType'=>'radio', 'tl_class' => 'w100'),
	'sql'				=> "binary(16) NULL"
);

$GLOBALS["TL_DCA"]["tl_page"]["fields"]["ixFloating"] = array(
	'label'				=> &$GLOBALS['TL_LANG']['tl_page']['ixFloating'],
	'default'			=> 'above',
	'exclude'			=> true,
	'inputType'			=> 'radioTable',
	'options'			=> array('above', 'left', 'right', 'below'),
	'eval'				=> array('cols'=>4, 'tl_class'=>'w50'),
	'reference'			=> &$GLOBALS['TL_LANG']['MSC'],
	'sql'				=> "varchar(32) NOT NULL default ''"
);

$GLOBALS["TL_DCA"]["tl_page"]["fields"]["ixAlternate"] = array(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['alt'],
	'exclude'                 => true,
	'search'                  => true,
	'inputType'               => 'text',
	'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
	'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_page']['fields']['ixPageNaviText'] = array(
	'label'				=> &$GLOBALS['TL_LANG']['tl_page']['ixPageNaviText'],
	'inputType'			=> 'textarea',
	'eval'				=> array('mandatory'=>false, 'rte'=>'tinyMCE', 'helpwizard'=>true, 'tl_class' => 'clr w100'),
	'explanation'		=> 'insertTags',
	'sql'				=> "mediumtext NULL"
);



?>