<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   zixBigDropDown
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright Ixtensa 2015
 */

/**
 * Namespace
 */
namespace IXTENSA;

class zixBigDropDown extends \Backend {

	public function addBigDropDown($id) {
		
		// import database
		$this->import('Database');
		$result = $this->Database->prepare("SELECT p2.* FROM tl_page p1 LEFT JOIN tl_page p2 ON p1.id=p2.pid where p2.id=? AND p1.addBigDropDown='1'")->execute($id);

		$arrResult = $result->fetchAllAssoc();

		if(count($arrResult) == 0){
			return false;
		}
		
		return true;

	} // function end
	
} // Class end
?>