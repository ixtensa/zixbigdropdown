<?php

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_page']['extra_legend'] = 'Big Drop-Down Navigation hinzufügen';


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_page']['addBigDropDown'] = array('Big Drop-Down Navigation hinzufügen','Wenn Sie diesen Checkbox aktivieren, wird an dieser Seite ein Big Drop-Down Navigation hinzugefügt. Sie müssen an alle Unterseiten dieser Seite ein Bild und Text eingeben.');
$GLOBALS['TL_LANG']['tl_page']['ixPageNaviImage'] = array('Bild für Big Drop-Down Navigation', '/files/userImages/navigation');
$GLOBALS['TL_LANG']['tl_page']['ixPageNaviText'] = array('Text für Big Drop-Down Navigation', 'Ein bis zwei kurze Sätze');
$GLOBALS['TL_LANG']['tl_page']['ixFloating'] = array('Bildausrichtung', 'Bitte legen Sie fest, wie das Bild ausgerichtet werden soll. Sie sollten die Ausrichtung bei den Seiten untereinander nicht mischen.');
$GLOBALS['TL_LANG']['tl_page']['alt'] = array('Alternativer Text', 'Hier können Sie einen alternativen Text für das Bild eingeben (<i>alt</i>-Attribut).')

?>