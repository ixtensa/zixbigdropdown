<?php

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_page']['extra_legend']   = 'Add big Drop-Down Navigation';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_page']['addBigDropDown'] = array('Add big Drop-Down navigation','If You activate this checkbox, you will add on this page a big Drop-Down navigation. You must enter for all subpages of this page an image and text');
$GLOBALS['TL_LANG']['tl_page']['ixPageNaviImage'] = array('Image for big Drop-Down navigation', '/files/userImages/navigation');
$GLOBALS['TL_LANG']['tl_page']['ixPageNaviText'] = array('Text for big Drop-Down navigation', 'Few short sentences');
$GLOBALS['TL_LANG']['tl_page']['ixFloating'] = array('Image alignment', 'Please specify how to align the image. You should not mix alignment with each other in other pages.');
$GLOBALS['TL_LANG']['tl_page']['alt'] = array('Alternate text', 'Here you can enter an alternate text for the image (<i>alt</i> attribute).')

?>